RSpec.describe 'Categories', type: :system do
  let!(:taxonomy) { create(:taxonomy, name: "Categories") }
  let!(:taxon1) { create(:taxon, taxonomy: taxonomy, name: "Bags") }
  let!(:taxon2) { create(:taxon, taxonomy: taxonomy, name: "Mugs") }

  let!(:product1) do
    create(:product, taxons: [taxon1], name: "RUBY ON RAILS TOTE")
  end
  let!(:taxonomies) { Spree::Taxonomy.all.includes(:root) }
  let!(:other_product) { create(:product, name: "Ruby on Rails Stein") }

  describe "正常系" do
    context "カテゴリーサイドバーの表示" do
      before do
        visit potepan_category_path(taxon1.id)
      end

      example "サイドバーにカテゴリ別に表示され、選択した商品が表示される" do
        taxon1.move_to_child_of(taxonomy.root)
        taxon2.move_to_child_of(taxonomy.root)
        within("ul.side-nav") do
          expect(page).to have_content taxonomy.name
        end
        within("div.productBox") do
          expect(page).to have_content product1.name
          expect(page).not_to have_content other_product.name
        end
      end

      example "商品の画像をクリックすると商品詳細ページへ遷移し、リンクからカテゴリのページに戻る" do
        click_link product1.name
        expect(page).to have_content product1.name
        expect(page).to have_content product1.display_price.to_s
        click_link "一覧ページへ戻る"
        expect(current_path).to eq potepan_category_path(taxon1.id)
      end
    end
  end
end
