RSpec.describe Potepan::ProductsController, type: :controller do
  let(:product) { create(:product) }

  describe "GET #show" do
    before do
      get :show, params: { id: product.id }
    end

    example "GETリクエストでshowアクションが表示される" do
      expect(response).to be_successful
    end

    example "200レスポンスを返すこと" do
      expect(response).to have_http_status "200"
    end

    example "@productという変数が正しく取得できている" do
      expect(assigns(:product)).to eq product
    end
  end
end
